from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from .models import GroceryItem

def index(request):
	groceryitem_list = GroceryItem.objects.all()
	context = {'groceryitem_list' : groceryitem_list}
	return render(request, "django_practice/index.html", context)

def groceryitem(request, groceryitem_id):
	response = "You are viewing the detials of %s"
	return HttpResponse(response %groceryitem_id)
